﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPiServer.Research.Translation4.Common
{
    public class TranslationException : System.Exception
    {
        public TranslationException(string message)
            : base(message)
        {
            
        }
    }
}
